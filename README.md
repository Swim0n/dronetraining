# Autonomous drone training in AirSim #

This project contains a modified version of [Microsoft's AirSim](https://github.com/Microsoft/AirSim) and python scripts using [Keras](https://keras.io/) 
to train and test a deep learning neural network model for autonomously controlling a drone.
