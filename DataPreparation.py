import os
import Cooking

RAW_DATA_DIR = 'data_raw/'

COOKED_DATA_DIR = 'data_cooked/'

DATA_FOLDERS = ['Safe flying', 'Safe flying 2', 'Safe flying 3', 'Safe flying 4', 'Safe flying 5', 'Safe flying 6',
                'Safe flying 7', 'Risky flying', 'Risky flying 2', 'Very safe flying', 'Very safe flying 2',
                'Walls and corners', 'Circling obstacle left', 'Circling obstacle left 2', 'Circling obstacle right',
                'Circling obstacle right 2']

train_eval_test_split = [0.74, 0.23, 0.03]
full_path_raw_folders = [os.path.join(RAW_DATA_DIR, f) for f in DATA_FOLDERS]
Cooking.cook(full_path_raw_folders, COOKED_DATA_DIR, train_eval_test_split)