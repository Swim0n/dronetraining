// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AIRSIM_SimModeWorldMultiRotor_generated_h
#error "SimModeWorldMultiRotor.generated.h already included, missing '#pragma once' in SimModeWorldMultiRotor.h"
#endif
#define AIRSIM_SimModeWorldMultiRotor_generated_h

#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_RPC_WRAPPERS
#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASimModeWorldMultiRotor(); \
	friend AIRSIM_API class UClass* Z_Construct_UClass_ASimModeWorldMultiRotor(); \
public: \
	DECLARE_CLASS(ASimModeWorldMultiRotor, ASimModeWorldBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/AirSim"), NO_API) \
	DECLARE_SERIALIZER(ASimModeWorldMultiRotor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASimModeWorldMultiRotor(); \
	friend AIRSIM_API class UClass* Z_Construct_UClass_ASimModeWorldMultiRotor(); \
public: \
	DECLARE_CLASS(ASimModeWorldMultiRotor, ASimModeWorldBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/AirSim"), NO_API) \
	DECLARE_SERIALIZER(ASimModeWorldMultiRotor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASimModeWorldMultiRotor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASimModeWorldMultiRotor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASimModeWorldMultiRotor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASimModeWorldMultiRotor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASimModeWorldMultiRotor(ASimModeWorldMultiRotor&&); \
	NO_API ASimModeWorldMultiRotor(const ASimModeWorldMultiRotor&); \
public:


#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASimModeWorldMultiRotor(ASimModeWorldMultiRotor&&); \
	NO_API ASimModeWorldMultiRotor(const ASimModeWorldMultiRotor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASimModeWorldMultiRotor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASimModeWorldMultiRotor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASimModeWorldMultiRotor)


#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_PRIVATE_PROPERTY_OFFSET
#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_12_PROLOG
#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_PRIVATE_PROPERTY_OFFSET \
	Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_RPC_WRAPPERS \
	Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_INCLASS \
	Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_PRIVATE_PROPERTY_OFFSET \
	Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_INCLASS_NO_PURE_DECLS \
	Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Blocks_Plugins_AirSim_Source_Multirotor_SimModeWorldMultiRotor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
