// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "AirSim.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1AirSim() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API class UClass* Z_Construct_UClass_AGameModeBase();
	ENGINE_API class UClass* Z_Construct_UClass_ACameraActor();
	ENGINE_API class UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
	COREUOBJECT_API class UClass* Z_Construct_UClass_UObject();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();
	ENGINE_API class UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	PHYSXVEHICLES_API class UClass* Z_Construct_UClass_AWheeledVehicle();
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FColor();
	ENGINE_API class UClass* Z_Construct_UClass_UAudioComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UTextRenderComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	PHYSXVEHICLES_API class UClass* Z_Construct_UClass_UVehicleWheel();
	ENGINE_API class UClass* Z_Construct_UClass_APawn();
	ENGINE_API class UClass* Z_Construct_UClass_URotatingMovementComponent_NoRegister();
	UMG_API class UClass* Z_Construct_UClass_UUserWidget();
	ENGINE_API class UClass* Z_Construct_UClass_AHUD();

	AIRSIM_API class UEnum* Z_Construct_UEnum_AirSim_LogDebugLevel();
	AIRSIM_API class UFunction* Z_Construct_UFunction_UAirBlueprintLib_LogMessage();
	AIRSIM_API class UClass* Z_Construct_UClass_UAirBlueprintLib_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_UAirBlueprintLib();
	AIRSIM_API class UClass* Z_Construct_UClass_AAirSimGameMode_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_AAirSimGameMode();
	AIRSIM_API class UClass* Z_Construct_UClass_APIPCamera_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_APIPCamera();
	AIRSIM_API class UClass* Z_Construct_UClass_UManualPoseController_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_UManualPoseController();
	AIRSIM_API class UEnum* Z_Construct_UEnum_AirSim_ECameraDirectorMode();
	AIRSIM_API class UFunction* Z_Construct_UFunction_ACameraDirector_getMode();
	AIRSIM_API class UFunction* Z_Construct_UFunction_ACameraDirector_setMode();
	AIRSIM_API class UClass* Z_Construct_UClass_ACameraDirector_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_ACameraDirector();
	AIRSIM_API class UClass* Z_Construct_UClass_ACarPawn_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_ACarPawn();
	AIRSIM_API class UClass* Z_Construct_UClass_UCarWheelFront_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_UCarWheelFront();
	AIRSIM_API class UClass* Z_Construct_UClass_UCarWheelRear_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_UCarWheelRear();
	AIRSIM_API class UClass* Z_Construct_UClass_AFlyingPawn_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_AFlyingPawn();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_getHelpContainerVisibility();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_getRecordButtonVisibility();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_getReportContainerVisibility();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_getSubwindowVisibility();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_initializeForPlay();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_onToggleRecordingButtonClick();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_setHelpContainerVisibility();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_setRecordButtonVisibility();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_setReportContainerVisibility();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_setReportText();
	AIRSIM_API class UFunction* Z_Construct_UFunction_USimHUDWidget_setSubwindowVisibility();
	AIRSIM_API class UClass* Z_Construct_UClass_USimHUDWidget_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_USimHUDWidget();
	AIRSIM_API class UFunction* Z_Construct_UFunction_ASimModeBase_toggleRecording();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimModeBase_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimModeBase();
	AIRSIM_API class UEnum* Z_Construct_UEnum_AirSim_ESimulatorMode();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimHUD_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimHUD();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimModeWorldBase_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimModeWorldBase();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimModeCar_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimModeCar();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimModeWorldMultiRotor_NoRegister();
	AIRSIM_API class UClass* Z_Construct_UClass_ASimModeWorldMultiRotor();
	AIRSIM_API class UPackage* Z_Construct_UPackage__Script_AirSim();
static UEnum* LogDebugLevel_StaticEnum()
{
	extern AIRSIM_API class UPackage* Z_Construct_UPackage__Script_AirSim();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern AIRSIM_API class UEnum* Z_Construct_UEnum_AirSim_LogDebugLevel();
		Singleton = GetStaticEnum(Z_Construct_UEnum_AirSim_LogDebugLevel, Z_Construct_UPackage__Script_AirSim(), TEXT("LogDebugLevel"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_LogDebugLevel(LogDebugLevel_StaticEnum, TEXT("/Script/AirSim"), TEXT("LogDebugLevel"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_AirSim_LogDebugLevel()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_AirSim();
		extern uint32 Get_Z_Construct_UEnum_AirSim_LogDebugLevel_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("LogDebugLevel"), 0, Get_Z_Construct_UEnum_AirSim_LogDebugLevel_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("LogDebugLevel"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("LogDebugLevel::Informational"), 0);
			EnumNames.Emplace(TEXT("LogDebugLevel::Success"), 1);
			EnumNames.Emplace(TEXT("LogDebugLevel::Failure"), 2);
			EnumNames.Emplace(TEXT("LogDebugLevel::Unimportant"), 3);
			EnumNames.Emplace(TEXT("LogDebugLevel::LogDebugLevel_MAX"), 4);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("LogDebugLevel");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("Failure.DisplayName"), TEXT("Failure"));
			MetaData->SetValue(ReturnEnum, TEXT("Informational.DisplayName"), TEXT("Informational"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("AirBlueprintLib.h"));
			MetaData->SetValue(ReturnEnum, TEXT("Success.DisplayName"), TEXT("Success"));
			MetaData->SetValue(ReturnEnum, TEXT("Unimportant.DisplayName"), TEXT("Unimportant"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_AirSim_LogDebugLevel_CRC() { return 491884948U; }
	void UAirBlueprintLib::StaticRegisterNativesUAirBlueprintLib()
	{
		UClass* Class = UAirBlueprintLib::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "LogMessage", (Native)&UAirBlueprintLib::execLogMessage },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_UAirBlueprintLib_LogMessage()
	{
		struct AirBlueprintLib_eventLogMessage_Parms
		{
			FString prefix;
			FString suffix;
			LogDebugLevel level;
			float persist_sec;
		};
		UObject* Outer=Z_Construct_UClass_UAirBlueprintLib();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("LogMessage"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(AirBlueprintLib_eventLogMessage_Parms));
			UProperty* NewProp_persist_sec = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("persist_sec"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(persist_sec, AirBlueprintLib_eventLogMessage_Parms), 0x0010000000000080);
			UProperty* NewProp_level = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("level"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(level, AirBlueprintLib_eventLogMessage_Parms), 0x0010000000000080, Z_Construct_UEnum_AirSim_LogDebugLevel());
			UProperty* NewProp_level_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_level, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			UProperty* NewProp_suffix = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("suffix"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(suffix, AirBlueprintLib_eventLogMessage_Parms), 0x0010000000000080);
			UProperty* NewProp_prefix = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("prefix"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(prefix, AirBlueprintLib_eventLogMessage_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Utils"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_persist_sec"), TEXT("60.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("AirBlueprintLib.h"));
			MetaData->SetValue(NewProp_suffix, TEXT("NativeConst"), TEXT(""));
			MetaData->SetValue(NewProp_prefix, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAirBlueprintLib_NoRegister()
	{
		return UAirBlueprintLib::StaticClass();
	}
	UClass* Z_Construct_UClass_UAirBlueprintLib()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBlueprintFunctionLibrary();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = UAirBlueprintLib::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20000080;

				OuterClass->LinkChild(Z_Construct_UFunction_UAirBlueprintLib_LogMessage());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UAirBlueprintLib_LogMessage(), "LogMessage"); // 2404479392
				static TCppClassTypeInfo<TCppClassTypeTraits<UAirBlueprintLib> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AirBlueprintLib.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("AirBlueprintLib.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAirBlueprintLib, 4009211652);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAirBlueprintLib(Z_Construct_UClass_UAirBlueprintLib, &UAirBlueprintLib::StaticClass, TEXT("/Script/AirSim"), TEXT("UAirBlueprintLib"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAirBlueprintLib);
	void AAirSimGameMode::StaticRegisterNativesAAirSimGameMode()
	{
	}
	UClass* Z_Construct_UClass_AAirSimGameMode_NoRegister()
	{
		return AAirSimGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AAirSimGameMode()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameModeBase();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = AAirSimGameMode::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900288;


				static TCppClassTypeInfo<TCppClassTypeTraits<AAirSimGameMode> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AirSimGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("AirSimGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAirSimGameMode, 3830783574);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAirSimGameMode(Z_Construct_UClass_AAirSimGameMode, &AAirSimGameMode::StaticClass, TEXT("/Script/AirSim"), TEXT("AAirSimGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAirSimGameMode);
	void APIPCamera::StaticRegisterNativesAPIPCamera()
	{
	}
	UClass* Z_Construct_UClass_APIPCamera_NoRegister()
	{
		return APIPCamera::StaticClass();
	}
	UClass* Z_Construct_UClass_APIPCamera()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACameraActor();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = APIPCamera::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_noise_material_static_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("noise_material_static_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(noise_material_static_, APIPCamera), 0x0040000000000000, Z_Construct_UClass_UMaterial_NoRegister());
				UProperty* NewProp_noise_materials_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("noise_materials_"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(noise_materials_, APIPCamera), 0x0040000000000000);
				UProperty* NewProp_noise_materials__Inner = new(EC_InternalUseOnlyConstructor, NewProp_noise_materials_, TEXT("noise_materials_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister());
				UProperty* NewProp_camera_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("camera_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(camera_, APIPCamera), 0x0040000000080008, Z_Construct_UClass_UCameraComponent_NoRegister());
				UProperty* NewProp_render_targets_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("render_targets_"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(render_targets_, APIPCamera), 0x0040000000000000);
				UProperty* NewProp_render_targets__Inner = new(EC_InternalUseOnlyConstructor, NewProp_render_targets_, TEXT("render_targets_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UClass_UTextureRenderTarget2D_NoRegister());
				UProperty* NewProp_captures_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("captures_"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(captures_, APIPCamera), 0x0040008000000008);
				UProperty* NewProp_captures__Inner = new(EC_InternalUseOnlyConstructor, NewProp_captures_, TEXT("captures_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000080008, Z_Construct_UClass_USceneCaptureComponent2D_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<APIPCamera> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Input Rendering"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("PIPCamera.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("PIPCamera.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(NewProp_noise_material_static_, TEXT("ModuleRelativePath"), TEXT("PIPCamera.h"));
				MetaData->SetValue(NewProp_noise_materials_, TEXT("ModuleRelativePath"), TEXT("PIPCamera.h"));
				MetaData->SetValue(NewProp_noise_materials_, TEXT("ToolTip"), TEXT("TMap<int, UMaterialInstanceDynamic*> noise_materials_;\nbelow is needed because TMap doesn't work with UPROPERTY, but we do have -ve index"));
				MetaData->SetValue(NewProp_camera_, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_camera_, TEXT("ModuleRelativePath"), TEXT("PIPCamera.h"));
				MetaData->SetValue(NewProp_render_targets_, TEXT("ModuleRelativePath"), TEXT("PIPCamera.h"));
				MetaData->SetValue(NewProp_captures_, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_captures_, TEXT("ModuleRelativePath"), TEXT("PIPCamera.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(APIPCamera, 2973109618);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APIPCamera(Z_Construct_UClass_APIPCamera, &APIPCamera::StaticClass, TEXT("/Script/AirSim"), TEXT("APIPCamera"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APIPCamera);
	void UManualPoseController::StaticRegisterNativesUManualPoseController()
	{
	}
	UClass* Z_Construct_UClass_UManualPoseController_NoRegister()
	{
		return UManualPoseController::StaticClass();
	}
	UClass* Z_Construct_UClass_UManualPoseController()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UObject();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = UManualPoseController::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20100080;


				static TCppClassTypeInfo<TCppClassTypeTraits<UManualPoseController> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("ManualPoseController.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("ManualPoseController.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UManualPoseController, 3917000863);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UManualPoseController(Z_Construct_UClass_UManualPoseController, &UManualPoseController::StaticClass, TEXT("/Script/AirSim"), TEXT("UManualPoseController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UManualPoseController);
static UEnum* ECameraDirectorMode_StaticEnum()
{
	extern AIRSIM_API class UPackage* Z_Construct_UPackage__Script_AirSim();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern AIRSIM_API class UEnum* Z_Construct_UEnum_AirSim_ECameraDirectorMode();
		Singleton = GetStaticEnum(Z_Construct_UEnum_AirSim_ECameraDirectorMode, Z_Construct_UPackage__Script_AirSim(), TEXT("ECameraDirectorMode"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECameraDirectorMode(ECameraDirectorMode_StaticEnum, TEXT("/Script/AirSim"), TEXT("ECameraDirectorMode"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_AirSim_ECameraDirectorMode()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_AirSim();
		extern uint32 Get_Z_Construct_UEnum_AirSim_ECameraDirectorMode_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECameraDirectorMode"), 0, Get_Z_Construct_UEnum_AirSim_ECameraDirectorMode_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ECameraDirectorMode"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("ECameraDirectorMode::CAMERA_DIRECTOR_MODE_FPV"), 1);
			EnumNames.Emplace(TEXT("ECameraDirectorMode::CAMERA_DIRECTOR_MODE_GROUND_OBSERVER"), 2);
			EnumNames.Emplace(TEXT("ECameraDirectorMode::CAMERA_DIRECTOR_MODE_FLY_WITH_ME"), 3);
			EnumNames.Emplace(TEXT("ECameraDirectorMode::CAMERA_DIRECTOR_MODE_MANUAL"), 4);
			EnumNames.Emplace(TEXT("ECameraDirectorMode::CAMERA_DIRECTOR_MODE_SPRINGARM_CHASE"), 5);
			EnumNames.Emplace(TEXT("ECameraDirectorMode::CAMREA_DIRECTOR_MODE_BACKUP"), 6);
			EnumNames.Emplace(TEXT("ECameraDirectorMode::CAMREA_DIRECTOR_MODE_NODISPLAY"), 7);
			EnumNames.Emplace(TEXT("ECameraDirectorMode::ECameraDirectorMode_MAX"), 8);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("ECameraDirectorMode");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("CAMERA_DIRECTOR_MODE_FLY_WITH_ME.DisplayName"), TEXT("FlyWithMe"));
			MetaData->SetValue(ReturnEnum, TEXT("CAMERA_DIRECTOR_MODE_FPV.DisplayName"), TEXT("FPV"));
			MetaData->SetValue(ReturnEnum, TEXT("CAMERA_DIRECTOR_MODE_GROUND_OBSERVER.DisplayName"), TEXT("GroundObserver"));
			MetaData->SetValue(ReturnEnum, TEXT("CAMERA_DIRECTOR_MODE_MANUAL.DisplayName"), TEXT("Manual"));
			MetaData->SetValue(ReturnEnum, TEXT("CAMERA_DIRECTOR_MODE_SPRINGARM_CHASE.DisplayName"), TEXT("SpringArmChase"));
			MetaData->SetValue(ReturnEnum, TEXT("CAMREA_DIRECTOR_MODE_BACKUP.DisplayName"), TEXT("Backup"));
			MetaData->SetValue(ReturnEnum, TEXT("CAMREA_DIRECTOR_MODE_NODISPLAY.DisplayName"), TEXT("No Display"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("CameraDirector.h"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_AirSim_ECameraDirectorMode_CRC() { return 2160838393U; }
	void ACameraDirector::StaticRegisterNativesACameraDirector()
	{
		UClass* Class = ACameraDirector::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "getMode", (Native)&ACameraDirector::execgetMode },
			{ "setMode", (Native)&ACameraDirector::execsetMode },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 2);
	}
	UFunction* Z_Construct_UFunction_ACameraDirector_getMode()
	{
		struct CameraDirector_eventgetMode_Parms
		{
			ECameraDirectorMode ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ACameraDirector();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("getMode"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(CameraDirector_eventgetMode_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(ReturnValue, CameraDirector_eventgetMode_Parms), 0x0010000000000580, Z_Construct_UEnum_AirSim_ECameraDirectorMode());
			UProperty* NewProp_ReturnValue_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_ReturnValue, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Modes"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("CameraDirector.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ACameraDirector_setMode()
	{
		struct CameraDirector_eventsetMode_Parms
		{
			ECameraDirectorMode mode;
		};
		UObject* Outer=Z_Construct_UClass_ACameraDirector();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("setMode"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(CameraDirector_eventsetMode_Parms));
			UProperty* NewProp_mode = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("mode"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(mode, CameraDirector_eventsetMode_Parms), 0x0010000000000080, Z_Construct_UEnum_AirSim_ECameraDirectorMode());
			UProperty* NewProp_mode_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_mode, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Modes"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("CameraDirector.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACameraDirector_NoRegister()
	{
		return ACameraDirector::StaticClass();
	}
	UClass* Z_Construct_UClass_ACameraDirector()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = ACameraDirector::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_ACameraDirector_getMode());
				OuterClass->LinkChild(Z_Construct_UFunction_ACameraDirector_setMode());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_manual_pose_controller_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("manual_pose_controller_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(manual_pose_controller_, ACameraDirector), 0x0040000000000000, Z_Construct_UClass_UManualPoseController_NoRegister());
				UProperty* NewProp_SpringArm = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SpringArm"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SpringArm, ACameraDirector), 0x00100000000b001d, Z_Construct_UClass_USpringArmComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ACameraDirector_getMode(), "getMode"); // 3705731719
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ACameraDirector_setMode(), "setMode"); // 3847755882
				static TCppClassTypeInfo<TCppClassTypeTraits<ACameraDirector> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("CameraDirector.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("CameraDirector.h"));
				MetaData->SetValue(NewProp_manual_pose_controller_, TEXT("ModuleRelativePath"), TEXT("CameraDirector.h"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("ModuleRelativePath"), TEXT("CameraDirector.h"));
				MetaData->SetValue(NewProp_SpringArm, TEXT("ToolTip"), TEXT("Spring arm that will offset the camera"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACameraDirector, 197064784);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACameraDirector(Z_Construct_UClass_ACameraDirector, &ACameraDirector::StaticClass, TEXT("/Script/AirSim"), TEXT("ACameraDirector"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACameraDirector);
	void ACarPawn::StaticRegisterNativesACarPawn()
	{
	}
	UClass* Z_Construct_UClass_ACarPawn_NoRegister()
	{
		return ACarPawn::StaticClass();
	}
	UClass* Z_Construct_UClass_ACarPawn()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AWheeledVehicle();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = ACarPawn::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20800080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_GearDisplayReverseColor = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GearDisplayReverseColor"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(GearDisplayReverseColor, ACarPawn), 0x0010000000030015, Z_Construct_UScriptStruct_FColor());
				UProperty* NewProp_GearDisplayColor = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GearDisplayColor"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(GearDisplayColor, ACarPawn), 0x0010000000030015, Z_Construct_UScriptStruct_FColor());
				UProperty* NewProp_GearDisplayString = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GearDisplayString"), RF_Public|RF_Transient|RF_MarkAsNative) UTextProperty(CPP_PROPERTY_BASE(GearDisplayString, ACarPawn), 0x0010000000030015);
				UProperty* NewProp_SpeedDisplayString = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SpeedDisplayString"), RF_Public|RF_Transient|RF_MarkAsNative) UTextProperty(CPP_PROPERTY_BASE(SpeedDisplayString, ACarPawn), 0x0010000000030015);
				UProperty* NewProp_EngineSoundComponent = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EngineSoundComponent"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(EngineSoundComponent, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_UAudioComponent_NoRegister());
				UProperty* NewProp_InCarGear = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InCarGear"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InCarGear, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_UTextRenderComponent_NoRegister());
				UProperty* NewProp_InCarSpeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InCarSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InCarSpeed, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_UTextRenderComponent_NoRegister());
				UProperty* NewProp_InternalCamera5 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCamera5"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCamera5, ACarPawn), 0x0040000000030015, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_InternalCamera4 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCamera4"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCamera4, ACarPawn), 0x0040000000030015, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_InternalCamera3 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCamera3"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCamera3, ACarPawn), 0x0040000000030015, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_InternalCamera2 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCamera2"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCamera2, ACarPawn), 0x0040000000030015, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_InternalCamera1 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCamera1"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCamera1, ACarPawn), 0x0040000000030015, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_InternalCameraBase5 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCameraBase5"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCameraBase5, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_USceneComponent_NoRegister());
				UProperty* NewProp_InternalCameraBase4 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCameraBase4"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCameraBase4, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_USceneComponent_NoRegister());
				UProperty* NewProp_InternalCameraBase3 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCameraBase3"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCameraBase3, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_USceneComponent_NoRegister());
				UProperty* NewProp_InternalCameraBase2 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCameraBase2"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCameraBase2, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_USceneComponent_NoRegister());
				UProperty* NewProp_InternalCameraBase1 = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("InternalCameraBase1"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(InternalCameraBase1, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_USceneComponent_NoRegister());
				UProperty* NewProp_Camera = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Camera"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Camera, ACarPawn), 0x00400000000b001d, Z_Construct_UClass_UCameraComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->ClassConfigName = FName(TEXT("Game"));
				static TCppClassTypeInfo<TCppClassTypeTraits<ACarPawn> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_GearDisplayReverseColor, TEXT("Category"), TEXT("Display"));
				MetaData->SetValue(NewProp_GearDisplayReverseColor, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_GearDisplayReverseColor, TEXT("ToolTip"), TEXT("The color of the incar gear text when in reverse"));
				MetaData->SetValue(NewProp_GearDisplayColor, TEXT("Category"), TEXT("Display"));
				MetaData->SetValue(NewProp_GearDisplayColor, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_GearDisplayColor, TEXT("ToolTip"), TEXT("The color of the incar gear text in forward gears"));
				MetaData->SetValue(NewProp_GearDisplayString, TEXT("Category"), TEXT("Display"));
				MetaData->SetValue(NewProp_GearDisplayString, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_GearDisplayString, TEXT("ToolTip"), TEXT("The current gear as a string (R,N, 1,2 etc)"));
				MetaData->SetValue(NewProp_SpeedDisplayString, TEXT("Category"), TEXT("Display"));
				MetaData->SetValue(NewProp_SpeedDisplayString, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_SpeedDisplayString, TEXT("ToolTip"), TEXT("The current speed as a string eg 10 km/h"));
				MetaData->SetValue(NewProp_EngineSoundComponent, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_EngineSoundComponent, TEXT("Category"), TEXT("Display"));
				MetaData->SetValue(NewProp_EngineSoundComponent, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_EngineSoundComponent, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_EngineSoundComponent, TEXT("ToolTip"), TEXT("Audio component for the engine sound"));
				MetaData->SetValue(NewProp_InCarGear, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InCarGear, TEXT("Category"), TEXT("Display"));
				MetaData->SetValue(NewProp_InCarGear, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_InCarGear, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InCarGear, TEXT("ToolTip"), TEXT("Text component for the In-Car gear"));
				MetaData->SetValue(NewProp_InCarSpeed, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InCarSpeed, TEXT("Category"), TEXT("Display"));
				MetaData->SetValue(NewProp_InCarSpeed, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_InCarSpeed, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InCarSpeed, TEXT("ToolTip"), TEXT("Text component for the In-Car speed"));
				MetaData->SetValue(NewProp_InternalCamera5, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCamera5, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCamera5, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCamera4, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCamera4, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCamera4, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCamera3, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCamera3, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCamera3, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCamera2, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCamera2, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCamera2, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCamera1, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCamera1, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCamera1, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCamera1, TEXT("ToolTip"), TEXT("Camera component for the In-Car view"));
				MetaData->SetValue(NewProp_InternalCameraBase5, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase5, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCameraBase5, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase5, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCameraBase4, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase4, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCameraBase4, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase4, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCameraBase3, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase3, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCameraBase3, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase3, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCameraBase2, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase2, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCameraBase2, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase2, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCameraBase1, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase1, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_InternalCameraBase1, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_InternalCameraBase1, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_InternalCameraBase1, TEXT("ToolTip"), TEXT("SCene component for the In-Car view origin"));
				MetaData->SetValue(NewProp_Camera, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_Camera, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_Camera, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_Camera, TEXT("ModuleRelativePath"), TEXT("Car/CarPawn.h"));
				MetaData->SetValue(NewProp_Camera, TEXT("ToolTip"), TEXT("Camera component that will be our viewpoint"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACarPawn, 2539380549);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACarPawn(Z_Construct_UClass_ACarPawn, &ACarPawn::StaticClass, TEXT("/Script/AirSim"), TEXT("ACarPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACarPawn);
	void UCarWheelFront::StaticRegisterNativesUCarWheelFront()
	{
	}
	UClass* Z_Construct_UClass_UCarWheelFront_NoRegister()
	{
		return UCarWheelFront::StaticClass();
	}
	UClass* Z_Construct_UClass_UCarWheelFront()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UVehicleWheel();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = UCarWheelFront::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20800080;


				static TCppClassTypeInfo<TCppClassTypeTraits<UCarWheelFront> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Car/CarWheelFront.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Car/CarWheelFront.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCarWheelFront, 375669448);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCarWheelFront(Z_Construct_UClass_UCarWheelFront, &UCarWheelFront::StaticClass, TEXT("/Script/AirSim"), TEXT("UCarWheelFront"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCarWheelFront);
	void UCarWheelRear::StaticRegisterNativesUCarWheelRear()
	{
	}
	UClass* Z_Construct_UClass_UCarWheelRear_NoRegister()
	{
		return UCarWheelRear::StaticClass();
	}
	UClass* Z_Construct_UClass_UCarWheelRear()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UVehicleWheel();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = UCarWheelRear::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20800080;


				static TCppClassTypeInfo<TCppClassTypeTraits<UCarWheelRear> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Car/CarWheelRear.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Car/CarWheelRear.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCarWheelRear, 2684574953);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCarWheelRear(Z_Construct_UClass_UCarWheelRear, &UCarWheelRear::StaticClass, TEXT("/Script/AirSim"), TEXT("UCarWheelRear"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCarWheelRear);
	void AFlyingPawn::StaticRegisterNativesAFlyingPawn()
	{
	}
	UClass* Z_Construct_UClass_AFlyingPawn_NoRegister()
	{
		return AFlyingPawn::StaticClass();
	}
	UClass* Z_Construct_UClass_AFlyingPawn()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APawn();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = AFlyingPawn::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_rotating_movements_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("rotating_movements_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(rotating_movements_, AFlyingPawn), 0x0040000000080008, Z_Construct_UClass_URotatingMovementComponent_NoRegister());
				NewProp_rotating_movements_->ArrayDim = CPP_ARRAY_DIM(rotating_movements_, AFlyingPawn);
				UProperty* NewProp_fpv_camera_bottom_center_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("fpv_camera_bottom_center_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(fpv_camera_bottom_center_, AFlyingPawn), 0x0040000000000000, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_fpv_camera_back_center_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("fpv_camera_back_center_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(fpv_camera_back_center_, AFlyingPawn), 0x0040000000000000, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_fpv_camera_front_center_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("fpv_camera_front_center_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(fpv_camera_front_center_, AFlyingPawn), 0x0040000000000000, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_fpv_camera_front_right_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("fpv_camera_front_right_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(fpv_camera_front_right_, AFlyingPawn), 0x0040000000000000, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_fpv_camera_front_left_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("fpv_camera_front_left_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(fpv_camera_front_left_, AFlyingPawn), 0x0040000000000000, Z_Construct_UClass_APIPCamera_NoRegister());
				UProperty* NewProp_RotatorFactor = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("RotatorFactor"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(RotatorFactor, AFlyingPawn), 0x0010000000000005);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<AFlyingPawn> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Multirotor/FlyingPawn.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Multirotor/FlyingPawn.h"));
				MetaData->SetValue(NewProp_rotating_movements_, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_rotating_movements_, TEXT("ModuleRelativePath"), TEXT("Multirotor/FlyingPawn.h"));
				MetaData->SetValue(NewProp_fpv_camera_bottom_center_, TEXT("ModuleRelativePath"), TEXT("Multirotor/FlyingPawn.h"));
				MetaData->SetValue(NewProp_fpv_camera_back_center_, TEXT("ModuleRelativePath"), TEXT("Multirotor/FlyingPawn.h"));
				MetaData->SetValue(NewProp_fpv_camera_front_center_, TEXT("ModuleRelativePath"), TEXT("Multirotor/FlyingPawn.h"));
				MetaData->SetValue(NewProp_fpv_camera_front_right_, TEXT("ModuleRelativePath"), TEXT("Multirotor/FlyingPawn.h"));
				MetaData->SetValue(NewProp_fpv_camera_front_left_, TEXT("ModuleRelativePath"), TEXT("Multirotor/FlyingPawn.h"));
				MetaData->SetValue(NewProp_RotatorFactor, TEXT("Category"), TEXT("Debugging"));
				MetaData->SetValue(NewProp_RotatorFactor, TEXT("ModuleRelativePath"), TEXT("Multirotor/FlyingPawn.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFlyingPawn, 3904701913);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFlyingPawn(Z_Construct_UClass_AFlyingPawn, &AFlyingPawn::StaticClass, TEXT("/Script/AirSim"), TEXT("AFlyingPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFlyingPawn);
	static FName NAME_USimHUDWidget_getHelpContainerVisibility = FName(TEXT("getHelpContainerVisibility"));
	bool USimHUDWidget::getHelpContainerVisibility()
	{
		SimHUDWidget_eventgetHelpContainerVisibility_Parms Parms;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_getHelpContainerVisibility),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_getRecordButtonVisibility = FName(TEXT("getRecordButtonVisibility"));
	bool USimHUDWidget::getRecordButtonVisibility()
	{
		SimHUDWidget_eventgetRecordButtonVisibility_Parms Parms;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_getRecordButtonVisibility),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_getReportContainerVisibility = FName(TEXT("getReportContainerVisibility"));
	bool USimHUDWidget::getReportContainerVisibility()
	{
		SimHUDWidget_eventgetReportContainerVisibility_Parms Parms;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_getReportContainerVisibility),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_getSubwindowVisibility = FName(TEXT("getSubwindowVisibility"));
	int32 USimHUDWidget::getSubwindowVisibility(int32 window_index)
	{
		SimHUDWidget_eventgetSubwindowVisibility_Parms Parms;
		Parms.window_index=window_index;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_getSubwindowVisibility),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_initializeForPlay = FName(TEXT("initializeForPlay"));
	bool USimHUDWidget::initializeForPlay()
	{
		SimHUDWidget_eventinitializeForPlay_Parms Parms;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_initializeForPlay),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_setHelpContainerVisibility = FName(TEXT("setHelpContainerVisibility"));
	bool USimHUDWidget::setHelpContainerVisibility(bool is_visible)
	{
		SimHUDWidget_eventsetHelpContainerVisibility_Parms Parms;
		Parms.is_visible=is_visible ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_setHelpContainerVisibility),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_setRecordButtonVisibility = FName(TEXT("setRecordButtonVisibility"));
	bool USimHUDWidget::setRecordButtonVisibility(bool is_visible)
	{
		SimHUDWidget_eventsetRecordButtonVisibility_Parms Parms;
		Parms.is_visible=is_visible ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_setRecordButtonVisibility),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_setReportContainerVisibility = FName(TEXT("setReportContainerVisibility"));
	bool USimHUDWidget::setReportContainerVisibility(bool is_visible)
	{
		SimHUDWidget_eventsetReportContainerVisibility_Parms Parms;
		Parms.is_visible=is_visible ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_setReportContainerVisibility),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_setReportText = FName(TEXT("setReportText"));
	bool USimHUDWidget::setReportText(const FString& text)
	{
		SimHUDWidget_eventsetReportText_Parms Parms;
		Parms.text=text;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_setReportText),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_USimHUDWidget_setSubwindowVisibility = FName(TEXT("setSubwindowVisibility"));
	bool USimHUDWidget::setSubwindowVisibility(int32 window_index, bool is_visible, UTextureRenderTarget2D* render_target)
	{
		SimHUDWidget_eventsetSubwindowVisibility_Parms Parms;
		Parms.window_index=window_index;
		Parms.is_visible=is_visible ? true : false;
		Parms.render_target=render_target;
		ProcessEvent(FindFunctionChecked(NAME_USimHUDWidget_setSubwindowVisibility),&Parms);
		return !!Parms.ReturnValue;
	}
	void USimHUDWidget::StaticRegisterNativesUSimHUDWidget()
	{
		UClass* Class = USimHUDWidget::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "onToggleRecordingButtonClick", (Native)&USimHUDWidget::execonToggleRecordingButtonClick },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_getHelpContainerVisibility()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("getHelpContainerVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08080800, 65535, sizeof(SimHUDWidget_eventgetHelpContainerVisibility_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventgetHelpContainerVisibility_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventgetHelpContainerVisibility_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventgetHelpContainerVisibility_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_getRecordButtonVisibility()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("getRecordButtonVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08020800, 65535, sizeof(SimHUDWidget_eventgetRecordButtonVisibility_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventgetRecordButtonVisibility_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventgetRecordButtonVisibility_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventgetRecordButtonVisibility_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_getReportContainerVisibility()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("getReportContainerVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08080800, 65535, sizeof(SimHUDWidget_eventgetReportContainerVisibility_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventgetReportContainerVisibility_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventgetReportContainerVisibility_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventgetReportContainerVisibility_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_getSubwindowVisibility()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("getSubwindowVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08020800, 65535, sizeof(SimHUDWidget_eventgetSubwindowVisibility_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(CPP_PROPERTY_BASE(ReturnValue, SimHUDWidget_eventgetSubwindowVisibility_Parms), 0x0010000000000580);
			UProperty* NewProp_window_index = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("window_index"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(CPP_PROPERTY_BASE(window_index, SimHUDWidget_eventgetSubwindowVisibility_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_initializeForPlay()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("initializeForPlay"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08020800, 65535, sizeof(SimHUDWidget_eventinitializeForPlay_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventinitializeForPlay_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventinitializeForPlay_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventinitializeForPlay_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_onToggleRecordingButtonClick()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("onToggleRecordingButtonClick"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Event handler"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_setHelpContainerVisibility()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("setHelpContainerVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08080800, 65535, sizeof(SimHUDWidget_eventsetHelpContainerVisibility_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventsetHelpContainerVisibility_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventsetHelpContainerVisibility_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventsetHelpContainerVisibility_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(is_visible, SimHUDWidget_eventsetHelpContainerVisibility_Parms, bool);
			UProperty* NewProp_is_visible = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("is_visible"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(is_visible, SimHUDWidget_eventsetHelpContainerVisibility_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(is_visible, SimHUDWidget_eventsetHelpContainerVisibility_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_setRecordButtonVisibility()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("setRecordButtonVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08020800, 65535, sizeof(SimHUDWidget_eventsetRecordButtonVisibility_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventsetRecordButtonVisibility_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventsetRecordButtonVisibility_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventsetRecordButtonVisibility_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(is_visible, SimHUDWidget_eventsetRecordButtonVisibility_Parms, bool);
			UProperty* NewProp_is_visible = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("is_visible"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(is_visible, SimHUDWidget_eventsetRecordButtonVisibility_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(is_visible, SimHUDWidget_eventsetRecordButtonVisibility_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_setReportContainerVisibility()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("setReportContainerVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08080800, 65535, sizeof(SimHUDWidget_eventsetReportContainerVisibility_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventsetReportContainerVisibility_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventsetReportContainerVisibility_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventsetReportContainerVisibility_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(is_visible, SimHUDWidget_eventsetReportContainerVisibility_Parms, bool);
			UProperty* NewProp_is_visible = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("is_visible"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(is_visible, SimHUDWidget_eventsetReportContainerVisibility_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(is_visible, SimHUDWidget_eventsetReportContainerVisibility_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_setReportText()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("setReportText"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08080800, 65535, sizeof(SimHUDWidget_eventsetReportText_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventsetReportText_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventsetReportText_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventsetReportText_Parms), sizeof(bool), true);
			UProperty* NewProp_text = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("text"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(text, SimHUDWidget_eventsetReportText_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
			MetaData->SetValue(NewProp_text, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USimHUDWidget_setSubwindowVisibility()
	{
		UObject* Outer=Z_Construct_UClass_USimHUDWidget();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("setSubwindowVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08020800, 65535, sizeof(SimHUDWidget_eventsetSubwindowVisibility_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimHUDWidget_eventsetSubwindowVisibility_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimHUDWidget_eventsetSubwindowVisibility_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimHUDWidget_eventsetSubwindowVisibility_Parms), sizeof(bool), true);
			UProperty* NewProp_render_target = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("render_target"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(render_target, SimHUDWidget_eventsetSubwindowVisibility_Parms), 0x0010000000000080, Z_Construct_UClass_UTextureRenderTarget2D_NoRegister());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(is_visible, SimHUDWidget_eventsetSubwindowVisibility_Parms, bool);
			UProperty* NewProp_is_visible = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("is_visible"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(is_visible, SimHUDWidget_eventsetSubwindowVisibility_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(is_visible, SimHUDWidget_eventsetSubwindowVisibility_Parms), sizeof(bool), true);
			UProperty* NewProp_window_index = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("window_index"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(CPP_PROPERTY_BASE(window_index, SimHUDWidget_eventsetSubwindowVisibility_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("C++ Interface"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("below are implemented in Blueprint. The return value is forced to be\nbool even when not needed because of Unreal quirk that if return value\nis not there then below are treated as events instead of overridable functions"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USimHUDWidget_NoRegister()
	{
		return USimHUDWidget::StaticClass();
	}
	UClass* Z_Construct_UClass_USimHUDWidget()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UUserWidget();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = USimHUDWidget::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20B01080;

				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_getHelpContainerVisibility());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_getRecordButtonVisibility());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_getReportContainerVisibility());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_getSubwindowVisibility());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_initializeForPlay());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_onToggleRecordingButtonClick());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_setHelpContainerVisibility());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_setRecordButtonVisibility());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_setReportContainerVisibility());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_setReportText());
				OuterClass->LinkChild(Z_Construct_UFunction_USimHUDWidget_setSubwindowVisibility());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_getHelpContainerVisibility(), "getHelpContainerVisibility"); // 1580887076
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_getRecordButtonVisibility(), "getRecordButtonVisibility"); // 1916563153
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_getReportContainerVisibility(), "getReportContainerVisibility"); // 2744254021
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_getSubwindowVisibility(), "getSubwindowVisibility"); // 4148958328
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_initializeForPlay(), "initializeForPlay"); // 449019499
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_onToggleRecordingButtonClick(), "onToggleRecordingButtonClick"); // 1815948888
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_setHelpContainerVisibility(), "setHelpContainerVisibility"); // 2330722875
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_setRecordButtonVisibility(), "setRecordButtonVisibility"); // 2211725937
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_setReportContainerVisibility(), "setReportContainerVisibility"); // 136781943
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_setReportText(), "setReportText"); // 3885123031
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USimHUDWidget_setSubwindowVisibility(), "setSubwindowVisibility"); // 2658305369
				static TCppClassTypeInfo<TCppClassTypeTraits<USimHUDWidget> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("SimHUD/SimHUDWidget.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUDWidget.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(USimHUDWidget, 556629092);
	static FCompiledInDefer Z_CompiledInDefer_UClass_USimHUDWidget(Z_Construct_UClass_USimHUDWidget, &USimHUDWidget::StaticClass, TEXT("/Script/AirSim"), TEXT("USimHUDWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USimHUDWidget);
	void ASimModeBase::StaticRegisterNativesASimModeBase()
	{
		UClass* Class = ASimModeBase::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "toggleRecording", (Native)&ASimModeBase::exectoggleRecording },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_ASimModeBase_toggleRecording()
	{
		struct SimModeBase_eventtoggleRecording_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASimModeBase();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("toggleRecording"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SimModeBase_eventtoggleRecording_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SimModeBase_eventtoggleRecording_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SimModeBase_eventtoggleRecording_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SimModeBase_eventtoggleRecording_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Recording"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("SimMode/SimModeBase.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASimModeBase_NoRegister()
	{
		return ASimModeBase::StaticClass();
	}
	UClass* Z_Construct_UClass_ASimModeBase()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = ASimModeBase::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_ASimModeBase_toggleRecording());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(EnableReport, ASimModeBase, bool);
				UProperty* NewProp_EnableReport = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EnableReport"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(EnableReport, ASimModeBase), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(EnableReport, ASimModeBase), sizeof(bool), true);
				UProperty* NewProp_CameraDirector = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CameraDirector"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CameraDirector, ASimModeBase), 0x0010000000000005, Z_Construct_UClass_ACameraDirector_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASimModeBase_toggleRecording(), "toggleRecording"); // 1972677999
				static TCppClassTypeInfo<TCppClassTypeTraits<ASimModeBase> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("SimMode/SimModeBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("SimMode/SimModeBase.h"));
				MetaData->SetValue(NewProp_EnableReport, TEXT("Category"), TEXT("Debugging"));
				MetaData->SetValue(NewProp_EnableReport, TEXT("ModuleRelativePath"), TEXT("SimMode/SimModeBase.h"));
				MetaData->SetValue(NewProp_CameraDirector, TEXT("Category"), TEXT("Refs"));
				MetaData->SetValue(NewProp_CameraDirector, TEXT("ModuleRelativePath"), TEXT("SimMode/SimModeBase.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASimModeBase, 785320512);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASimModeBase(Z_Construct_UClass_ASimModeBase, &ASimModeBase::StaticClass, TEXT("/Script/AirSim"), TEXT("ASimModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASimModeBase);
static UEnum* ESimulatorMode_StaticEnum()
{
	extern AIRSIM_API class UPackage* Z_Construct_UPackage__Script_AirSim();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern AIRSIM_API class UEnum* Z_Construct_UEnum_AirSim_ESimulatorMode();
		Singleton = GetStaticEnum(Z_Construct_UEnum_AirSim_ESimulatorMode, Z_Construct_UPackage__Script_AirSim(), TEXT("ESimulatorMode"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESimulatorMode(ESimulatorMode_StaticEnum, TEXT("/Script/AirSim"), TEXT("ESimulatorMode"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_AirSim_ESimulatorMode()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_AirSim();
		extern uint32 Get_Z_Construct_UEnum_AirSim_ESimulatorMode_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESimulatorMode"), 0, Get_Z_Construct_UEnum_AirSim_ESimulatorMode_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ESimulatorMode"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("ESimulatorMode::SIM_MODE_HIL"), 0);
			EnumNames.Emplace(TEXT("ESimulatorMode::SIM_MODE_MAX"), 1);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("ESimulatorMode");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUD.h"));
			MetaData->SetValue(ReturnEnum, TEXT("SIM_MODE_HIL.DisplayName"), TEXT("Hardware-in-loop"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_AirSim_ESimulatorMode_CRC() { return 2241004364U; }
	void ASimHUD::StaticRegisterNativesASimHUD()
	{
	}
	UClass* Z_Construct_UClass_ASimHUD_NoRegister()
	{
		return ASimHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_ASimHUD()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AHUD();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = ASimHUD::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2090028C;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_simmode_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("simmode_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(simmode_, ASimHUD), 0x0040000000000000, Z_Construct_UClass_ASimModeBase_NoRegister());
				UProperty* NewProp_widget_ = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("widget_"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(widget_, ASimHUD), 0x0040000000080008, Z_Construct_UClass_USimHUDWidget_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->ClassConfigName = FName(TEXT("Game"));
				static TCppClassTypeInfo<TCppClassTypeTraits<ASimHUD> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Rendering Actor Input Replication"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("SimHUD/SimHUD.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUD.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(NewProp_simmode_, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUD.h"));
				MetaData->SetValue(NewProp_widget_, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_widget_, TEXT("ModuleRelativePath"), TEXT("SimHUD/SimHUD.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASimHUD, 1778140020);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASimHUD(Z_Construct_UClass_ASimHUD, &ASimHUD::StaticClass, TEXT("/Script/AirSim"), TEXT("ASimHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASimHUD);
	void ASimModeWorldBase::StaticRegisterNativesASimModeWorldBase()
	{
	}
	UClass* Z_Construct_UClass_ASimModeWorldBase_NoRegister()
	{
		return ASimModeWorldBase::StaticClass();
	}
	UClass* Z_Construct_UClass_ASimModeWorldBase()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASimModeBase();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = ASimModeWorldBase::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_manual_pose_controller = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("manual_pose_controller"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(manual_pose_controller, ASimModeWorldBase), 0x0020080000000000, Z_Construct_UClass_UManualPoseController_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASimModeWorldBase> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("SimMode/SimModeWorldBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("SimMode/SimModeWorldBase.h"));
				MetaData->SetValue(NewProp_manual_pose_controller, TEXT("ModuleRelativePath"), TEXT("SimMode/SimModeWorldBase.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASimModeWorldBase, 2994595989);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASimModeWorldBase(Z_Construct_UClass_ASimModeWorldBase, &ASimModeWorldBase::StaticClass, TEXT("/Script/AirSim"), TEXT("ASimModeWorldBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASimModeWorldBase);
	void ASimModeCar::StaticRegisterNativesASimModeCar()
	{
	}
	UClass* Z_Construct_UClass_ASimModeCar_NoRegister()
	{
		return ASimModeCar::StaticClass();
	}
	UClass* Z_Construct_UClass_ASimModeCar()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASimModeBase();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = ASimModeCar::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


				static TCppClassTypeInfo<TCppClassTypeTraits<ASimModeCar> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Car/SimModeCar.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Car/SimModeCar.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASimModeCar, 4176088659);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASimModeCar(Z_Construct_UClass_ASimModeCar, &ASimModeCar::StaticClass, TEXT("/Script/AirSim"), TEXT("ASimModeCar"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASimModeCar);
	void ASimModeWorldMultiRotor::StaticRegisterNativesASimModeWorldMultiRotor()
	{
	}
	UClass* Z_Construct_UClass_ASimModeWorldMultiRotor_NoRegister()
	{
		return ASimModeWorldMultiRotor::StaticClass();
	}
	UClass* Z_Construct_UClass_ASimModeWorldMultiRotor()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASimModeWorldBase();
			Z_Construct_UPackage__Script_AirSim();
			OuterClass = ASimModeWorldMultiRotor::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


				static TCppClassTypeInfo<TCppClassTypeTraits<ASimModeWorldMultiRotor> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Multirotor/SimModeWorldMultiRotor.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Multirotor/SimModeWorldMultiRotor.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASimModeWorldMultiRotor, 1586488427);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASimModeWorldMultiRotor(Z_Construct_UClass_ASimModeWorldMultiRotor, &ASimModeWorldMultiRotor::StaticClass, TEXT("/Script/AirSim"), TEXT("ASimModeWorldMultiRotor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASimModeWorldMultiRotor);
	UPackage* Z_Construct_UPackage__Script_AirSim()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/AirSim")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0x534C8FA6;
			Guid.B = 0x54A76FB5;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
