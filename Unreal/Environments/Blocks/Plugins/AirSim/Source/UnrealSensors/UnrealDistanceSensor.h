// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

#pragma once

#include "common/Common.hpp"
#include "GameFramework/Actor.h"
#include "sensors/distance/DistanceSimple.hpp"

class UnrealDistanceSensor : public msr::airlib::DistanceSimple {
public:
    UnrealDistanceSensor(AActor* actor);
	void setWorld(UWorld* world) {
		world_ = world;
		debugLineEnabled = false;
	}

protected:
    virtual msr::airlib::real_T getRayLength(const msr::airlib::Pose& relativePose, const msr::airlib::Pose& dronePose) override;

private:
    using Vector3r = msr::airlib::Vector3r;
	using Quaternionr = msr::airlib::Quaternionr;
    using VectorMath = msr::airlib::VectorMath;


private:
    AActor* actor_;
	UWorld* world_;
	bool debugLineEnabled = false;
};