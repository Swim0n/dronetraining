// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

#include "UnrealDistanceSensor.h"
#include "AirBlueprintLib.h"
#include "common/Common.hpp"
#include "NedTransform.h"
//#include "LevelEditor.h"
//#include "Editor.h"
#include "Runtime/Engine/Classes/Engine/GameInstance.h"
#include "DrawDebugHelpers.h"

UnrealDistanceSensor::UnrealDistanceSensor(AActor* actor)
    : actor_(actor)
{
}

msr::airlib::real_T UnrealDistanceSensor::getRayLength(const msr::airlib::Pose& relativePose, const msr::airlib::Pose& dronePose)
{
    //update ray tracing
    Vector3r start = (relativePose + dronePose).position;

	Quaternionr q1(relativePose.orientation * Quaternionr(0.991, 0, 0, 0.131));
	q1.normalize();
	q1 = q1 * dronePose.orientation;
	q1.normalize();
	Quaternionr q2(relativePose.orientation * Quaternionr(0.998, 0, 0, 0.065));
	q2.normalize();
	q2 = q2 * dronePose.orientation;
	q2.normalize();
	Quaternionr q3(relativePose.orientation);
	q3 = q3 * dronePose.orientation;
	q3.normalize();
	Quaternionr q4(relativePose.orientation * Quaternionr(0.998, 0, 0, -0.065));
	q4.normalize();
	q4 = q4 * dronePose.orientation;
	q4.normalize();
	Quaternionr q5(relativePose.orientation * Quaternionr(0.991, 0, 0, -0.131));
	q5.normalize();
	q5 = q5 * dronePose.orientation;
	q5.normalize();

    Vector3r end1 = start + VectorMath::rotateVector(VectorMath::front(), q1, true) * getParams().max_distance;
	Vector3r end2 = start + VectorMath::rotateVector(VectorMath::front(), q2, true) * getParams().max_distance;
	Vector3r end3 = start + VectorMath::rotateVector(VectorMath::front(), q3, true) * getParams().max_distance;
	Vector3r end4 = start + VectorMath::rotateVector(VectorMath::front(), q4, true) * getParams().max_distance;
	Vector3r end5 = start + VectorMath::rotateVector(VectorMath::front(), q5, true) * getParams().max_distance;

	float distance;

    FHitResult dist_hit1 = FHitResult(ForceInit);
    bool is_hit1 = UAirBlueprintLib::GetObstacle(actor_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end1), dist_hit1);
    float distance1 = is_hit1 ? dist_hit1.Distance / 100.0f : getParams().max_distance;
	distance = distance1;

	FHitResult dist_hit2 = FHitResult(ForceInit);
	bool is_hit2 = UAirBlueprintLib::GetObstacle(actor_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end2), dist_hit2);
	float distance2 = is_hit2 ? dist_hit2.Distance / 100.0f : getParams().max_distance;
	if (distance2 < distance)
		distance = distance2;

	FHitResult dist_hit3 = FHitResult(ForceInit);
	bool is_hit3 = UAirBlueprintLib::GetObstacle(actor_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end3), dist_hit3);
	float distance3 = is_hit3 ? dist_hit3.Distance / 100.0f : getParams().max_distance;
	if (distance3 < distance)
		distance = distance3;

	FHitResult dist_hit4 = FHitResult(ForceInit);
	bool is_hit4 = UAirBlueprintLib::GetObstacle(actor_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end4), dist_hit4);
	float distance4 = is_hit4 ? dist_hit4.Distance / 100.0f : getParams().max_distance;
	if (distance4 < distance)
		distance = distance4;

	FHitResult dist_hit5 = FHitResult(ForceInit);
	bool is_hit5 = UAirBlueprintLib::GetObstacle(actor_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end5), dist_hit5);
	float distance5 = is_hit5 ? dist_hit5.Distance / 100.0f : getParams().max_distance;
	if (distance5 < distance)
		distance = distance5;

	if (debugLineEnabled) {
		DrawDebugLine(world_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end1), FColor(255, 0, 0, 1), false, 0.05f, 0, 1.3);
		DrawDebugLine(world_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end2), FColor(0, 255, 0, 1), false, 0.05f, 0, 1.3);
		DrawDebugLine(world_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end3), FColor(0, 0, 255, 1), false, 0.05f, 0, 1.3);
		DrawDebugLine(world_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end4), FColor(0, 255, 0, 1), false, 0.05f, 0, 1.3);
		DrawDebugLine(world_, NedTransform::toNeuUU(start), NedTransform::toNeuUU(end5), FColor(255, 0, 0, 1), false, 0.05f, 0, 1.3);
	}
	


    //FString hit_name = FString("None");
    //if (dist_hit.GetActor())
    //    hit_name=dist_hit.GetActor()->GetName();

    //UAirBlueprintLib::LogMessage(FString("Distance to "), hit_name+FString(": ")+FString::SanitizeFloat(distance), LogDebugLevel::Informational);

    return distance;
}
