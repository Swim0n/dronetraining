#include "SimModeWorldMultiRotor.h"
#include "ConstructorHelpers.h"
#include "AirBlueprintLib.h"
#include "vehicles/multirotor/controllers/DroneControllerBase.hpp"
#include "physics/PhysicsBody.hpp"
#include <memory>
#include "Logging/MessageLog.h"
#include "vehicles/multirotor/MultiRotorParamsFactory.hpp"
#include "UnrealSensors/UnrealSensorFactory.h"
#include "UnrealSensors/UnrealDistanceSensor.h"


ASimModeWorldMultiRotor::ASimModeWorldMultiRotor()
{
    static ConstructorHelpers::FClassFinder<APIPCamera> external_camera_class(TEXT("Blueprint'/AirSim/Blueprints/BP_PIPCamera'"));
    external_camera_class_ = external_camera_class.Succeeded() ? external_camera_class.Class : nullptr;
    static ConstructorHelpers::FClassFinder<ACameraDirector> camera_director_class(TEXT("Blueprint'/AirSim/Blueprints/BP_CameraDirector'"));
    camera_director_class_ = camera_director_class.Succeeded() ? camera_director_class.Class : nullptr;
    static ConstructorHelpers::FClassFinder<TMultiRotorPawn> vehicle_pawn_class(TEXT("Blueprint'/AirSim/Blueprints/BP_FlyingPawn'"));
    vehicle_pawn_class_ = vehicle_pawn_class.Succeeded() ? vehicle_pawn_class.Class : nullptr;
}

void ASimModeWorldMultiRotor::BeginPlay()
{
    Super::BeginPlay();

    //create control server
    for (const std::shared_ptr<VehicleConnectorBase>& vehicle_connector_ : fpv_vehicle_connectors_) {
        try {
            vehicle_connector_->startApiServer();
        }
        catch (std::exception& ex) {
            UAirBlueprintLib::LogMessageString("Cannot start RpcLib Server", ex.what(), LogDebugLevel::Failure);
        }
    }

}

void ASimModeWorldMultiRotor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    //stop physics thread before we dismental
    stopAsyncUpdator();

    for (const std::shared_ptr<VehicleConnectorBase>& vehicle_connector_ : fpv_vehicle_connectors_)
        vehicle_connector_->stopApiServer();

    //for (AActor* actor : spawned_actors_) {
    //    actor->Destroy();
    //}
    spawned_actors_.Empty();
    //fpv_vehicle_connectors_.Empty();
    CameraDirector = nullptr;

    Super::EndPlay(EndPlayReason);
}

VehiclePawnWrapper* ASimModeWorldMultiRotor::getFpvVehiclePawnWrapper()
{
    return fpv_vehicle_pawn_wrapper_;
}

void ASimModeWorldMultiRotor::setupVehiclesAndCamera(std::vector<VehiclePtr>& vehicles)
{
    //get player controller
    APlayerController* player_controller = this->GetWorld()->GetFirstPlayerController();
    FTransform actor_transform = player_controller->GetActorTransform();
    //put camera little bit above vehicle
    FTransform camera_transform(actor_transform.GetLocation() + FVector(-300, 0, 200));

    //we will either find external camera if it already exist in evironment or create one
    APIPCamera* external_camera;

    //find all BP camera directors in the environment
    {
        TArray<AActor*> camera_dirs;
        UAirBlueprintLib::FindAllActor<ACameraDirector>(this, camera_dirs);
        if (camera_dirs.Num() == 0) {
            //create director
            FActorSpawnParameters camera_spawn_params;
            camera_spawn_params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
            CameraDirector = this->GetWorld()->SpawnActor<ACameraDirector>(camera_director_class_, camera_transform, camera_spawn_params);
            CameraDirector->setFollowDistance(225);
            CameraDirector->setCameraRotationLagEnabled(false);
            CameraDirector->setFpvCameraIndex(0);
            CameraDirector->enableFlyWithMeMode();
            spawned_actors_.Add(CameraDirector);

            //create external camera required for the director
            external_camera = this->GetWorld()->SpawnActor<APIPCamera>(external_camera_class_, camera_transform, camera_spawn_params);
            spawned_actors_.Add(external_camera);
        }
        else {
            CameraDirector = static_cast<ACameraDirector*>(camera_dirs[0]);
            external_camera = CameraDirector->getExternalCamera();
        }
    }

    //find all vehicle pawns
    {
        TArray<AActor*> pawns;
        UAirBlueprintLib::FindAllActor<TMultiRotorPawn>(this, pawns);

        //if no vehicle pawns exists in environment
        if (pawns.Num() == 0) {
            //create vehicle pawn
            FActorSpawnParameters pawn_spawn_params;
            pawn_spawn_params.SpawnCollisionHandlingOverride =
                ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
            TMultiRotorPawn* spawned_pawn = this->GetWorld()->SpawnActor<TMultiRotorPawn>(
                vehicle_pawn_class_, actor_transform, pawn_spawn_params);

            spawned_actors_.Add(spawned_pawn);
            pawns.Add(spawned_pawn);
        }

        //set up vehicle pawns
        for (AActor* pawn : pawns)
        {
            //initialize each vehicle pawn we found
            TMultiRotorPawn* vehicle_pawn = static_cast<TMultiRotorPawn*>(pawn);
            vehicle_pawn->initializeForBeginPlay();

            //chose first pawn as FPV if none is designated as FPV
            VehiclePawnWrapper* wrapper = vehicle_pawn->getVehiclePawnWrapper();

            if (getSettings().enable_collision_passthrough)
                wrapper->getConfig().enable_passthrough_on_collisions = true;
            if (wrapper->getConfig().is_fpv_vehicle || fpv_vehicle_pawn_wrapper_ == nullptr)
                fpv_vehicle_pawn_wrapper_ = wrapper;

            //now create the connector for each pawn
            VehiclePtr vehicle = createVehicle(wrapper);
            if (vehicle != nullptr) {
                vehicles.push_back(vehicle);
                fpv_vehicle_connectors_.Add(vehicle);
            }
            //else we don't have vehicle for this pawn
        }
    }

    CameraDirector->initializeForBeginPlay(getInitialViewMode(), fpv_vehicle_pawn_wrapper_, external_camera);
}

void ASimModeWorldMultiRotor::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    getFpvVehiclePawnWrapper()->setLogLine(getLogString());
}


std::string ASimModeWorldMultiRotor::getLogString()
{
	using namespace std;
	MultiRotorConnector& mrc = static_cast<MultiRotorConnector&>(*fpv_vehicle_connectors_[0]);
	RCData data = mrc.getRCData();
	SimpleFlightQuadX& mrp = static_cast<SimpleFlightQuadX&>(*vehicle_params_[0]);

	static const SensorBase* s1;
	static const SensorBase* s2;
	static const SensorBase* s3;
	static const SensorBase* s4;
	static const SensorBase* s5;
	static const SensorBase* s6;

	if (!initialized) {
		s1 = mrp.getSensors().getByType(SensorBase::SensorType::Distance, 0);
		s2 = mrp.getSensors().getByType(SensorBase::SensorType::Distance, 1);
		s3 = mrp.getSensors().getByType(SensorBase::SensorType::Distance, 2);
		s4 = mrp.getSensors().getByType(SensorBase::SensorType::Distance, 3);
		s5 = mrp.getSensors().getByType(SensorBase::SensorType::Distance, 4);
		s6 = mrp.getSensors().getByType(SensorBase::SensorType::Distance, 5);
	}

	UnrealDistanceSensor& d1 = (UnrealDistanceSensor&)(*s1);
	UnrealDistanceSensor& d2 = (UnrealDistanceSensor&)(*s2);
	UnrealDistanceSensor& d3 = (UnrealDistanceSensor&)(*s3);
	UnrealDistanceSensor& d4 = (UnrealDistanceSensor&)(*s4);
	UnrealDistanceSensor& d5 = (UnrealDistanceSensor&)(*s5);
	UnrealDistanceSensor& d6 = (UnrealDistanceSensor&)(*s6);

	if (!initialized) {
		UWorld* world = this->GetWorld();
		/*d1.setPose(Pose(Vector3r(0, 0, -0.15), Quaternionr(0.793, 0, 0, 0.609)));
		d1.setWorld(world);
		d2.setPose(Pose(Vector3r(0, 0, -0.15), Quaternionr(0.924, 0, 0, 0.383)));
		d2.setWorld(world);
		d3.setPose(Pose(Vector3r(0, 0, -0.15), Quaternionr(0.991, 0, 0, 0.131)));
		d3.setWorld(world);
		d4.setPose(Pose(Vector3r(0, 0, -0.15), Quaternionr(0.991, 0, 0, -0.131)));
		d4.setWorld(world);
		d5.setPose(Pose(Vector3r(0, 0, -0.15), Quaternionr(0.924, 0, 0, -0.383)));
		d5.setWorld(world);
		d6.setPose(Pose(Vector3r(0, 0, -0.15), Quaternionr(0.793, 0, 0, -0.609)));
		d6.setWorld(world);*/
		d1.setPose(Pose(Vector3r(0, -0.36, -0.15), Quaternionr(0.793, 0, 0, -0.609)));
		d1.setWorld(world);
		d2.setPose(Pose(Vector3r(0.36, -0.36, -0.15), Quaternionr(0.924, 0, 0, -0.383)));
		d2.setWorld(world);
		d3.setPose(Pose(Vector3r(0.36, -0.2, -0.15), Quaternionr(0.998, 0, 0, -0.065)));
		d3.setWorld(world);
		d4.setPose(Pose(Vector3r(0.36, 0.2, -0.15), Quaternionr(0.998, 0, 0, 0.065)));
		d4.setWorld(world);
		d5.setPose(Pose(Vector3r(0.36, 0.36, -0.15), Quaternionr(0.924, 0, 0, 0.383)));
		d5.setWorld(world);
		d6.setPose(Pose(Vector3r(0, 0.36, -0.15), Quaternionr(0.793, 0, 0, 0.609)));
		d6.setWorld(world);
		initialized = true;
	}

	const msr::airlib::Kinematics::State* kinematics = getFpvVehiclePawnWrapper()->getTrueKinematics();
	uint64_t timestamp_millis = static_cast<uint64_t>(msr::airlib::ClockFactory::get()->nowNanos() / 1.0E6);

	std::string line;
	float xVel = kinematics->twist.linear.x();
	float yVel = kinematics->twist.linear.y();

	Quaternionr orientation = kinematics->pose.orientation;
	Vector2r rightVector = Vector2r(2 * (orientation.x()*orientation.z() + orientation.w()*orientation.y()), 2 * (orientation.y()*orientation.z() - orientation.w()*orientation.x()));
	rightVector.normalize();
	Vector2r forwardVector = Vector2r(1 - (2 * (orientation.y()*orientation.y() + orientation.z()*orientation.z())), 2 * (orientation.x()*orientation.y() + orientation.w()*orientation.z()));
	forwardVector.normalize();

	float forwardVel = forwardVector.x() * xVel + forwardVector.y() * yVel;
	float rightVel = rightVector.x() * xVel + rightVector.y() * yVel;

	line.append(std::to_string(timestamp_millis)).append("\t")
		.append(std::to_string(forwardVel)).append("\t")
		.append(std::to_string(rightVel)).append("\t")
		.append(std::to_string(data.pitch)).append("\t")
		.append(std::to_string(data.roll)).append("\t")
		.append(std::to_string(data.yaw)).append("\t")
		.append(std::to_string(d1.getOutput().distance)).append("\t")
		.append(std::to_string(d2.getOutput().distance)).append("\t")
		.append(std::to_string(d3.getOutput().distance)).append("\t")
		.append(std::to_string(d4.getOutput().distance)).append("\t")
		.append(std::to_string(d5.getOutput().distance)).append("\t")
		.append(std::to_string(d6.getOutput().distance)).append("\t");


	return line;

    //std::stringstream ss;
    //ss << timestamp_millis << "\t";
    //ss << kinematics.pose.position.x() << "\t" << kinematics.pose.position.y() << "\t" << kinematics.pose.position.z() << "\t";
    //ss << kinematics.pose.orientation.w() << "\t" << kinematics.pose.orientation.x() << "\t" << kinematics.pose.orientation.y() << "\t" << kinematics.pose.orientation.z() << "\t";
    //ss << "\n";
    //return ss.str();
}

void ASimModeWorldMultiRotor::createVehicles(std::vector<VehiclePtr>& vehicles)
{
    //find vehicles and cameras available in environment
    //if none available then we will create one
    setupVehiclesAndCamera(vehicles);
}

ASimModeWorldBase::VehiclePtr ASimModeWorldMultiRotor::createVehicle(VehiclePawnWrapper* wrapper)
{
    std::shared_ptr<UnrealSensorFactory> sensor_factory = std::make_shared<UnrealSensorFactory>(wrapper->getPawn());
    auto vehicle_params = MultiRotorParamsFactory::createConfig(wrapper->getVehicleConfigName(), sensor_factory);

    vehicle_params_.push_back(std::move(vehicle_params));

    std::shared_ptr<MultiRotorConnector> vehicle = std::make_shared<MultiRotorConnector>(
        wrapper, vehicle_params_.back().get(), getSettings().enable_rpc, getSettings().api_server_address,
            vehicle_params_.back()->getParams().api_server_port + vehicle_params_.size() - 1, manual_pose_controller
    );

    if (vehicle->getPhysicsBody() != nullptr)
        wrapper->setKinematics(&(static_cast<PhysicsBody*>(vehicle->getPhysicsBody())->getKinematics()));

    return std::static_pointer_cast<VehicleConnectorBase>(vehicle);
}


