from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model
from keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Dense, Lambda, Input, concatenate, LeakyReLU, PReLU
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import ELU
from keras.optimizers import Adam, SGD, Adamax, Nadam
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint, CSVLogger, EarlyStopping
import keras.backend as K
from keras.preprocessing import image

from keras_tqdm import TQDMNotebookCallback

import json
import os
import numpy as np
import pandas as pd
from Cooking import checkAndCreateDir
import h5py
from PIL import Image, ImageDraw
import math
import matplotlib.pyplot as plt

COOKED_DATA_DIR = 'data_cooked/'
MODEL_OUTPUT_DIR = 'model/'

train_dataset = h5py.File(os.path.join(COOKED_DATA_DIR, 'train.h5'), 'r')
eval_dataset = h5py.File(os.path.join(COOKED_DATA_DIR, 'eval.h5'), 'r')
test_dataset = h5py.File(os.path.join(COOKED_DATA_DIR, 'test.h5'), 'r')

num_train_examples = train_dataset['label'].shape[0]
num_eval_examples = eval_dataset['label'].shape[0]
num_test_examples = test_dataset['label'].shape[0]

state_input = Input(shape=(8,))

stack = Dense(50, activation='linear')(state_input)
stack = Dropout(0.2)(stack)
stack = PReLU()(stack)
stack = Dense(20, activation='linear')(stack)
stack = Dropout(0.2)(stack)
stack = PReLU()(stack)
stack = Dense(3, name='output')(stack)

adam = Nadam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
model = Model(inputs=state_input, outputs=stack)
model.compile(optimizer=adam, loss='mse')

model.summary()

print('Training on {0} samples'.format(num_train_examples))
print('Evaluating on {0} samples'.format(num_eval_examples))

plateau_callback = ReduceLROnPlateau(monitor="val_loss", factor=0.5, patience=3, min_lr=0.0001, verbose=1)
checkpoint_filepath = os.path.join(MODEL_OUTPUT_DIR, 'models', '{0}_model.{1}-{2}.h5'.format('model', '{epoch:02d}', '{val_loss:.7f}'))
checkAndCreateDir(checkpoint_filepath)
checkpoint_callback = ModelCheckpoint(checkpoint_filepath, save_best_only=True, verbose=1)
csv_callback = CSVLogger(os.path.join(MODEL_OUTPUT_DIR, 'training_log.csv'))
early_stopping_callback = EarlyStopping(monitor="val_loss", patience=10, verbose=1)

callbacks=[plateau_callback, csv_callback, checkpoint_callback, early_stopping_callback, TQDMNotebookCallback()]

def generate_from(dataset, batch_size):
    while 1:
        for i in range(0, dataset['previous_state'].shape[0], batch_size):
            yield dataset['previous_state'][i:i+batch_size], dataset['label'][i:i+batch_size]

batch_size = 32

train_generator = generate_from(train_dataset, batch_size)
eval_generator = generate_from(eval_dataset, batch_size)

#model.fit(train_dataset['previous_state'], train_dataset['label'], epochs=500, callbacks=callbacks,
#          validation_data=(eval_dataset['previous_state'], eval_dataset['label']), verbose=2, shuffle="batch")
model.fit_generator(train_generator, steps_per_epoch=num_train_examples//batch_size, epochs=500, callbacks=callbacks,
                    validation_data=eval_generator, validation_steps=num_eval_examples//batch_size, verbose=2)