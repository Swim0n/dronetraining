from keras.models import load_model
import numpy as np
from AirSimClient import *
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.5
config.gpu_options.visible_device_list = "0"
set_session(tf.Session(config=config))

# Set this to the path of the network model
MODEL_PATH = 'model/models/model_model.236-0.1854081.h5'
model = load_model(MODEL_PATH)

# Establish connection with AirSim
client = MultirotorClient()
client.confirmConnection()

MAX_VELOCITY = 1
MAX_HEIGHT = 0.9

SENSOR_ADJUSTMENT = -0.11

# Returns the X and Y velocities of the drone
def getVelocities():
    xVel = client.getMultirotorState().kinematics_true.linear_velocity.x_val  # X velocity in relation to the UE World
    yVel = client.getMultirotorState().kinematics_true.linear_velocity.y_val  # Y velocity in relation to the UE World
    orientation = client.getOrientation()  # Quaternion representation of the drone orientation in relation to the UE World

    rightVectorX = 2 * (orientation.x_val * orientation.z_val + orientation.w_val * orientation.y_val)
    rightVectorY = 2 * (orientation.y_val * orientation.z_val - orientation.w_val * orientation.x_val)
    rightVectorLength = np.sqrt(rightVectorX * rightVectorX + rightVectorY * rightVectorY)
    if rightVectorLength != 0:  # Normalize
        rightVectorX /= rightVectorLength
        rightVectorY /= rightVectorLength

    forwardVectorX = 1 - (2 * (orientation.y_val * orientation.y_val + orientation.z_val * orientation.z_val))
    forwardVectorY = 2 * (orientation.x_val * orientation.y_val + orientation.w_val * orientation.z_val)
    forwardVectorLength = np.sqrt(forwardVectorX * forwardVectorX + forwardVectorY * forwardVectorY)
    if forwardVectorLength != 0:  # Normalize
        forwardVectorX /= forwardVectorLength
        forwardVectorY /= forwardVectorLength

    forwardVel = forwardVectorX * xVel + forwardVectorY * yVel
    rightVel = rightVectorX * xVel + rightVectorY * yVel
    return [forwardVel, rightVel]

# Returns the distances measures by each sensor
def getSensors():
    data = client.getSensorData()
    s1 = (data.sensor1 / 2.5) + SENSOR_ADJUSTMENT
    s2 = (data.sensor2 / 2.5) + SENSOR_ADJUSTMENT
    s3 = (data.sensor3 / 2.5) + SENSOR_ADJUSTMENT
    s4 = (data.sensor4 / 2.5) + SENSOR_ADJUSTMENT   # Normalize input for network processing, adjusting sensor data
    s5 = (data.sensor5 / 2.5) + SENSOR_ADJUSTMENT   # to match real-life drone.
    s6 = (data.sensor6 / 2.5) + SENSOR_ADJUSTMENT
    return [s1, s2, s3, s4, s5, s6]

# If needed, adjusts the data predicted by the model to account for high velocities and heights etc.
def calculateNewRCData(predictedRCData, velocities):
    pitch = float(predictedRCData[0][0]) * 0.04
    roll = float(predictedRCData[0][1]) * 0.04      # De-normalize the output from [-1, 0] to the ranges used in training
    yaw = float(predictedRCData[0][2]) * 0.1
    throttle = 0.5945

    if np.sqrt(velocities[0]*velocities[0] + velocities[1]*velocities[1]) > MAX_VELOCITY:  # Avoid high velocities
        if velocities[0] > abs(velocities[1]):
            pitch = -0.04
        else:
            if velocities[1] > 0:
                roll = -0.04
            else:
                roll = 0.04
        print('Velocity too high, slowing down.')

    if velocities[0] < -0.08:  # Avoid flying backwards
        pitch = 0.02
        print('Backwards velocity detected, pitching forward.')

    if -client.getPosition().z_val > MAX_HEIGHT:  # Go lower if above max height
        throttle = 0.5925
        print('Drone above max height, descending slowly.')

    return [pitch, roll, yaw, throttle]

# Initialize an array for holding the input data
current_state = np.zeros((1,8))

# Hover the drone and have the model make a dummy prediction (first prediction takes a lot of time to complete)
client.setRCData(rcdata = RCData(pitch = 0, roll = 0, yaw = 0, throttle = 0.594, is_initialized = True, is_valid = True))
model.predict(current_state)
time.sleep(2)

while True:
    velocities = getVelocities()
    sensors = getSensors()

    current_state[0] = np.array([velocities[0], velocities[1], sensors[0], sensors[1],
                                sensors[2], sensors[3], sensors[4], sensors[5]])

    predictedRCData = model.predict(current_state)

    newRCData = calculateNewRCData(predictedRCData, velocities)

    client.setRCData(rcdata = RCData(pitch = newRCData[0], roll = newRCData[1], yaw = newRCData[2],
                                     throttle = newRCData[3], is_initialized = True, is_valid = True))

    time.sleep(0.1)