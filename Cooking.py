import random
import numpy as np
import pandas as pd
import sys
import os
import errno
import h5py


def checkAndCreateDir(full_path):
    """Checks if a given path exists and if not, creates the needed directories.
            Inputs:
                full_path: path to be checked
    """
    if not os.path.exists(os.path.dirname(full_path)):
        try:
            os.makedirs(os.path.dirname(full_path))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    
def splitTrainValidationAndTestData(all_data_mappings, split_ratio=(0.7, 0.2, 0.1)):
    """Simple function to create train, validation and test splits on the data.
            Inputs:
                all_data_mappings: mappings from the entire dataset
                split_ratio: (train, validation, test) split ratio

            Returns:
                train_data_mappings: mappings for training data
                validation_data_mappings: mappings for validation data
                test_data_mappings: mappings for test data

    """
    if round(sum(split_ratio), 5) != 1.0:
        print("Error: Your splitting ratio should add up to 1")
        sys.exit()

    train_split = int(len(all_data_mappings) * split_ratio[0])
    val_split = train_split + int(len(all_data_mappings) * split_ratio[1])

    train_data_mappings = all_data_mappings[0:train_split]
    validation_data_mappings = all_data_mappings[train_split:val_split]
    test_data_mappings = all_data_mappings[val_split:]

    return [train_data_mappings, validation_data_mappings, test_data_mappings]
    
def generateDataMapAirSim(folders):
    """ Data map generator for simulator(AirSim) data. Reads the driving_log csv file and returns a list of 'center camera image name - label(s)' tuples
           Inputs:
               folders: list of folders to collect data from

           Returns:
               mappings: All data mappings as a dictionary. Key is the image filepath, the values are a 2-tuple:
                   0 -> label(s) as a list of double
                   1 -> previous state as a list of double
    """
    senscount = 0
    ctrlcount = 0
    index = 0
    all_mappings = {}
    for folder in folders:
        print('Reading data from {0}...'.format(folder))
        current_df = pd.read_csv(os.path.join(folder, 'airsim_rec.txt'), sep='\t')
        
        for i in range(1, current_df.shape[0] - 1, 1):
            previous_state = list(current_df.iloc[i-1][['ForwardVelocity', 'SideVelocity', 'Sensor1',
                                                        'Sensor2', "Sensor3", 'Sensor4', 'Sensor5', 'Sensor6']])
            current_label = list((current_df.iloc[i][['Pitch', 'Roll', 'Yaw']] +
                                  current_df.iloc[i-1][['Pitch', 'Roll', 'Yaw']] +
                                  current_df.iloc[i+1][['Pitch', 'Roll', 'Yaw']]) / 3.0)

            previous_state[2] /= 2.39
            previous_state[3] /= 2.39
            previous_state[4] /= 2.39        # Normalize inputs for network processing
            previous_state[5] /= 2.39
            previous_state[6] /= 2.39
            previous_state[7] /= 2.39

            current_label[0] /= 0.04
            current_label[1] /= 0.04
            current_label[2] /= 0.1

            # Mirror the sensors, invert side velocity, invert roll and yaw. This doubles the amount of available samples.
            previous_state_mirrored = list([previous_state[0], -previous_state[1], previous_state[7], previous_state[6],
                                           previous_state[5], previous_state[4], previous_state[3], previous_state[2]])

            current_label_mirrored = list([current_label[0], -current_label[1], -current_label[2]])

            if (previous_state[2] > 0.97 and previous_state[3] > 0.97 and previous_state[4] > 0.97 and previous_state[5] > 0.97 and previous_state[6] > 0.97 and previous_state[7] > 0.97):
                if(np.random.uniform(low=0, high=1) > 0.95):  # Drop 95% of samples where all sensors have capped distances
                    all_mappings[str(index)] = (current_label, previous_state)
                    index += 1
                    all_mappings[str(index)] = (current_label_mirrored, previous_state_mirrored)
                    index += 1
                else:
                    senscount += 2
            elif (current_label[0] == 0 and current_label[1] == 0 and current_label[2] == 0):
                if (np.random.uniform(low=0, high=1) > 0.7):  # Drop 70% of samples where control input is 0
                    all_mappings[str(index)] = (current_label, previous_state)
                    index += 1
                    all_mappings[str(index)] = (current_label_mirrored, previous_state_mirrored)
                    index += 1
                else:
                    ctrlcount += 2
            else:
                all_mappings[str(index)] = (current_label, previous_state)
                index += 1
                all_mappings[str(index)] = (current_label_mirrored, previous_state_mirrored)
                index += 1
    
    mappings = [(key, all_mappings[key]) for key in all_mappings]
    print('{0} max-sensor-distance samples dropped'.format(senscount))
    print('{0} zero-control-input samples dropped'.format(ctrlcount))
    random.shuffle(mappings)
    
    return mappings

def generatorForH5py(data_mappings, chunk_size=32):
    """
    This function batches the data for saving to the H5 file
    """
    for chunk_id in range(0, len(data_mappings), chunk_size):
        # Data is expected to be a dict of <image: (label, previousious_state)>
        # Extract the parts
        data_chunk = data_mappings[chunk_id:chunk_id + chunk_size]
        if (len(data_chunk) == chunk_size):
            labels_chunk = np.asarray([b[0] for (a, b) in data_chunk])
            previous_state_chunk = np.asarray([b[1] for (a, b) in data_chunk])
            
            #Flatten and yield as tuple
            yield (labels_chunk.astype(float), previous_state_chunk.astype(float))
            if chunk_id + chunk_size > len(data_mappings):
                raise StopIteration
    raise StopIteration
    
def saveH5pyData(data_mappings, target_file_path):
    """
    Saves H5 data to file
    """
    chunk_size = 32
    gen = generatorForH5py(data_mappings,chunk_size)

    labels_chunk, previous_state_chunk = next(gen)
    row_count = labels_chunk.shape[0]

    checkAndCreateDir(target_file_path)
    with h5py.File(target_file_path, 'w') as f:

        # Initialize a resizable dataset to hold the output
        labels_chunk_maxshape = (None,) + labels_chunk.shape[1:]
        previous_state_maxshape = (None,) + previous_state_chunk.shape[1:]

        dset_labels = f.create_dataset('label', shape=labels_chunk.shape, maxshape=labels_chunk_maxshape,
                                       chunks=labels_chunk.shape, dtype=labels_chunk.dtype)
        
        dset_previous_state = f.create_dataset('previous_state', shape=previous_state_chunk.shape, maxshape=previous_state_maxshape,
                                       chunks=previous_state_chunk.shape, dtype=previous_state_chunk.dtype)

        dset_labels[:] = labels_chunk
        dset_previous_state[:] = previous_state_chunk

        for label_chunk, previous_state_chunk in gen:

            # Resize the dataset to accommodate the next chunk of rows
            dset_labels.resize(row_count + label_chunk.shape[0], axis=0)
            dset_previous_state.resize(row_count + previous_state_chunk.shape[0], axis=0)
            # Write the next chunk
            dset_labels[row_count:] = label_chunk
            dset_previous_state[row_count:] = previous_state_chunk

            # Increment the row count
            row_count += labels_chunk.shape[0]
            
            
def cook(folders, output_directory, train_eval_test_split):
    """ Primary function for data pre-processing. Reads and saves all data as h5 files.
            Inputs:
                folders: a list of all data folders
                output_directory: location for saving h5 files
                train_eval_test_split: dataset split ratio
    """
    output_files = [os.path.join(output_directory, f) for f in ['train.h5', 'eval.h5', 'test.h5']]
    if (any([os.path.isfile(f) for f in output_files])):
       print("Preprocessed data already exists at: {0}. Skipping preprocessing.".format(output_directory))

    else:
        all_data_mappings = generateDataMapAirSim(folders)
        
        split_mappings = splitTrainValidationAndTestData(all_data_mappings, split_ratio=train_eval_test_split)
        
        for i in range(0, len(split_mappings), 1):
            print('Processing {0}...'.format(output_files[i]))
            saveH5pyData(split_mappings[i], output_files[i])
            print('Finished saving {0}.'.format(output_files[i]))